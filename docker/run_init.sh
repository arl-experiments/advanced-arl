#python3 -m pip install -U -e '/workspace' --prefer-binary

git config --global --add safe.directory /palaestrai

cd /palaestrai || exit

# Unshallow shallow git clone (--depth 1)
# Source: https://stackoverflow.com/a/17937889
git fetch --unshallow
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin

git switch arl-discriminator
python3 -m pip install -U -e '/palaestrai[full-dev]' --prefer-binary
python3 -m pip install --no-deps -U -e git+https://gitlab.com/arl2/arl.git@development#egg=arl --prefer-binary