#!/bin/bash

export EXPERIMENT_FILE=${EXPERIMENT_FILE:-}
export EXPERIMENT_RUN_FILES=${EXPERIMENT_RUN_FILES:-/workspace/palaestrai-runfiles}

set -eu
set -o pipefail

cd /workspace

echo "*** Running arsenAI:"
(
    cd /workspace
    IFS=
    shopt -s nullglob
    experiment_files="/workspace/palaestrai-experiment-files/*.yml"
    if [ -n "$EXPERIMENT_FILE" ]; then
        experiment_files=$EXPERIMENT_FILE
    fi
    for f in $experiment_files; do
        echo "  * $f"
        arsenai generate "$f"
    done
)

echo "timescaledb:5432:carl:postgres:StoreTheMightyCARL" > ~/.pgpass
echo "timescaledb:5432:postgres:postgres:StoreTheMightyCARL" >> ~/.pgpass
chmod 0600 ~/.pgpass
if grep '^ *store_uri: *postgresql:[^@]\+@timescaledb/carl' /workspace/palaestrai.conf \
        && [ "$(psql \
	    -XtA \
	    -h timescaledb \
	    -U postgres \
	    -c "SELECT 1 FROM pg_database WHERE datname='carl'")" != '1' ];
then
    echo "*** Creating the database..."
    psql -U postgres -h timescaledb -c 'CREATE DATABASE carl;'
    palaestrai -c /workspace/palaestrai.conf database-create
fi

/carl/run_init.sh

exec /carl/start_experiment.sh