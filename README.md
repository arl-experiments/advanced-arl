# Adversarial Resilience Learning - Experiments

Adversarial Resilience Learning (ARL) describes both a methodology and
its reference implementation. In the ARL methodology, two (or more) competing
agents (“attacker” and “defender”) compete for control of a Critical
National Infrastructure (CNI). Through this competition, the two agents
learn robust strategies for a resilient control of the CNI. The research
group develops an advanced architecture on top of it, featuring higher-order
Deep Reinforcement Learning algorithms, extraction of learned strategies in
a non-standard logic via methods of Explainable Reinforcement Learning, and
learning from domain-expert knowledge.

This repository houses the experiments compatible with the ARL reference
implementation.

## ARL

The image containing the experiment file, can be built with:

```bash
docker build --no-cache -t carl -f docker/Dockerfile .
```

An experiment (here
`palaestrai-experiment-files/explain-carl-experiment-paper.yml`) can then
be started with:

```bash
docker run --rm --env EXPERIMENT_FILE=/workspace/palaestrai-experiment-files/explain-carl-experiment-paper.yml -d -e ROOTLESS=1 --name advanced_arl advanced_arl
```

and its logs observed with:

```bash
docker logs -f advanced_arl
```

Before actually starting the experiment, `arsenai generate`s the runfile
under `palaestrai-runfiles` from the experiment file
`palaestrai-experiment-files/explain-carl-experiment-paper.yml`.
Also, the package is installed in the container with pip by
the `docker/run_init.sh`-script. Then the generated runfiles are started by
palaestrai by calling the `docker/start_experiment.sh` script.